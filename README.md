This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## OpenWeatherMap

![alt text](http://i.piccy.info/i9/e90fbe083b521a5968c70cca97e50a6b/1547446279/12338/1289884/Screenshot_4.png)

### `git clone https://gitlab.com/andrew.velkov/openweathermap.git`

### `cd openweathermap`

### `yarn install`

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `yarn build`