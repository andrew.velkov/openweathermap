import * as type from 'constants/weather';
import API from 'config/api';

export const getWeather = (city, units) => {
  return {
    types: [type.GET_WEATHER, type.GET_WEATHER_SUCCESS, type.GET_WEATHER_ERROR],
    request: {
      method: 'GET',
      url: `${API.base_url}?q=${city}&appid=${API.api_key}&units=${units}`,
    },
  };
};
