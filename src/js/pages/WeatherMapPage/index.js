import React from 'react';
import WeatherMapContainer from 'containers/WeatherMapContainer';
import Grid from '@material-ui/core/Grid';

const WeatherMapPage = () => (
  <Grid container justify="center" item xs={12}>
    <Grid item xs={11} sm={11} md={10} lg={7}>
      <WeatherMapContainer />
    </Grid>
  </Grid>
);

export default WeatherMapPage;
