export const GET_WEATHER = 'weather/openweathermap/LOAD';
export const GET_WEATHER_SUCCESS = 'weather/openweathermap/LOAD_SUCCESS';
export const GET_WEATHER_ERROR = 'weather/openweathermap/LOAD_ERROR';
