import React from 'react';
import PropTypes from 'prop-types';

const WeatherList = ({ data, loaded, symbol }) => (
  <React.Fragment>
    {loaded && <ul>
      <li>
        <h4>{data.weather[0].description}</h4>
        <img src={`http://openweathermap.org/img/w/${data.weather[0].icon}.png`} alt=""/>
      </li>
      <li>temp: {data.main.temp} {symbol.name}</li>
      <li>humidity: {data.main.humidity}%</li>
      <li>pressure: {data.main.pressure} mb / {(data.main.pressure / 1.333).toFixed(2)} мм.рт.ст</li>
  </ul>}
  </React.Fragment>
);

WeatherList.propTypes = {
  loaded: PropTypes.bool,
  loading: PropTypes.bool,
};

export default WeatherList;
