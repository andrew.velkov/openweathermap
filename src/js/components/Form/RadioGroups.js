import React from 'react';
import PropTypes from 'prop-types';
import {
  FormControl, FormLabel, RadioGroup, FormControlLabel, Radio
} from '@material-ui/core';

const RadioGroups = ({ data, title, value, onChange }) => (
  <FormControl component="fieldset">
    <FormLabel component="legend">{title}</FormLabel>
    <RadioGroup
      name='radio'
      value={value}
      onChange={onChange}
      style={{display: 'inline-block'}}
    >
      {data.map(item => {
        return (
          <FormControlLabel
            key={item.id}
            value={item.value}
            label={item.name}
            control={<Radio color="primary" />}
            style={{paddingRight: '15px'}}
          />
        );
      })}
    </RadioGroup>
  </FormControl>
);

RadioGroups.propTypes = {
  onChange: PropTypes.func,
};

export default RadioGroups;
