import React from 'react';
import PropTypes from 'prop-types';
import { TextField, MenuItem } from '@material-ui/core';

const Selects = ({ data, title, value, onChange }) => (
  <TextField
    label={title}
    name='select'
    select
    fullWidth
    value={value}
    onChange={onChange}
  >
    {data.map((item) => {
      return (
        <MenuItem key={ item.id } value={ item.value }>
          {item.value}
        </MenuItem>
      );
    })}
  </TextField>
);

Selects.propTypes = {
  onChange: PropTypes.func,
};

export default Selects;
