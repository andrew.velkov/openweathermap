import { combineReducers } from 'redux';

import weathermap from './weathermap';

const reducers = combineReducers({
  get: combineReducers({
    weather: weathermap('openweathermap'),
  }),
});

export default reducers;
