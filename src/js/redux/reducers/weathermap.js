const initialState = {
  loaded: false,
  loading: false,
  data: [],
};

export default function weathermap(params = '') {
  return (state = initialState, action = {}) => {
    switch (action.type) {
      case `weather/${params}/LOAD`:
        return {
          ...state,
          loading: true,
          loaded: false,
        };
      case `weather/${params}/LOAD_SUCCESS`:
        return {
          ...state,
          loading: false,
          loaded: true,
          data: action.payload,
        };
      case `weather/${params}/LOAD_ERROR`:
        return {
          ...state,
          loading: false,
          loaded: false,
        };
      default:
        return state;
    }
  };
};