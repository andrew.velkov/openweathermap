const apiMiddleWare = () => next => action => {
  const { types, request: { method = 'GET', url }, status, ...rest } = action;

  if (!types) {
    return next(action);
  }

  const [REQUEST, SUCCESS, FAILURE] = types;

  next({ ...rest, type: REQUEST });

  return fetch(url, {
    method,
    // body: JSON.stringify(body),
  })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return response.json();
      }

      if (response.status === 401) {
        return next({ ...rest, type: FAILURE });
      }

      return response.json();
    })
    .then(data => next({
      ...rest,
      type: SUCCESS,
      payload: data,
    }))
    .catch(error => next({
      ...rest,
      type: FAILURE,
      payload: error.message,
    }));
};

export default apiMiddleWare;
