import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import WeatherMapPage from 'pages/WeatherMapPage';
import NotFoundPage from 'pages/NotFoundPage';

import history from './history';

export default class Routes extends Component {
  render() {
    return (
      <Router history={ history }>
        <Switch>
          <Route exact path='/' render={ (props) => <WeatherMapPage {...props} /> } />
          <Route component={ NotFoundPage } />
        </Switch>
      </Router>
    );
  }
}
