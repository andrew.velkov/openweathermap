import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';

import { getWeather } from 'actions';
import Fetching from 'components/Fetching';
import Select from 'components/Form/Selects';
import RadioGroup from 'components/Form/RadioGroups';
import WeatherList from 'components/WeatherList';

import cities from 'config/cities';
import metrics from 'config/metrics';

class WeatherMapContainer extends Component {
  static propTypes = {
    weather: PropTypes.object,
    getWeather: PropTypes.func,
  };

  state = {
    city: "London",
    metric: '',
  }

  componentDidMount() {
    const { city } = this.state;
    this.props.getWeather(city, '');
  }

  handleChangeCity = (e) => {
    const { metric } = this.state;

    this.setState({
      city: e.target.value,
    });
    this.props.getWeather(e.target.value, metric);
  }

  handleChangeMetric = (e) => {
    const { city } = this.state;

    this.setState({
      metric: e.target.value,
    });
    this.props.getWeather(city, e.target.value);
  }

  render() {
    const { data, loaded, loading } = this.props.weather;
    const { city, metric } = this.state;
    const metricSymbol = metrics.find(item => item.value === metric);

    return (
      <React.Fragment>
        <h2>Weather Map</h2>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={6}>
            <Select
              title='Select city'
              data={cities}
              value={city}
              onChange={this.handleChangeCity}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <RadioGroup
              title='Metrics'
              data={metrics}
              value={metric}
              onChange={this.handleChangeMetric}
            />
          </Grid>

          <Grid item xs={12}>
            <Fetching isFetching={loading}>
              <WeatherList
                loaded={loaded}
                data={data}
                symbol={metricSymbol}
              />
            </Fetching>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    weather: state.get.weather,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getWeather: (city, metric) => dispatch(getWeather(city, metric)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherMapContainer);
