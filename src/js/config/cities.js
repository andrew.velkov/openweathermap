const cities = [
  {
    id: 1,
    value: 'London',
  },
  {
    id: 2,
    value: 'Stockholm',
  },
  {
    id: 3,
    value: 'Kyiv',
  },
  {
    id: 4,
    value: 'Odessa',
  },
  {
    id: 5,
    value: 'New York',
  },
];

export default cities;