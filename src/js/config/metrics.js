const metrics = [
  {
    id: 1,
    value: '',
    name: 'K',
  },
  {
    id: 2,
    value: 'metric',
    name: '°C',
  },
  {
    id: 3,
    value: 'imperial',
    name: '°F',
  }
];

export default metrics;